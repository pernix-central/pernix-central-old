package com.pernix_central.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.pernix_central.domain.Participation;
import com.pernix_central.domain.ParticipationWrapper;
import com.pernix_central.domain.User;
import com.pernix_central.repository.ParticipationRepository;
import com.pernix_central.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Participation.
 */
@RestController
@RequestMapping("/api")
public class ParticipationResource {

    private final Logger log = LoggerFactory.getLogger(ParticipationResource.class);

    @Inject
    private ParticipationRepository participationRepository;

    /**
     * POST  /participations : Create a new participation.
     * @param participationWrapper an object containing the participation and a list of users that produce that participation
     * @return the ResponseEntity with status 201 (Created) and with body the new participation, or with status 400 (Bad Request) if the participation has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/participations",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity createParticipation(@RequestBody ParticipationWrapper participationWrapper) throws URISyntaxException {
        if (participationWrapper.getParticipation().getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("participation", "idexists", "A new participation cannot already have an ID")).body(null);
        }
        for(User user : participationWrapper.getUsers()) {
            Participation participationTpm = participationWrapper.getParticipation();
            participationTpm.setId(null);
            participationTpm.setUser(user);
            log.debug("REST request to save Participation : {}", participationTpm);
            participationRepository.save(participationTpm);
        }
        return ResponseEntity.ok().build();
    }

    /**
     * PUT  /participations : Updates an existing participation.
     *
     * @param participation the participation to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated participation,
     * or with status 400 (Bad Request) if the participation is not valid,
     * or with status 500 (Internal Server Error) if the participation couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/participations",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Participation> updateParticipation(@RequestBody ParticipationWrapper participationWrapper) throws URISyntaxException {
        log.debug("REST request to update Participation : {}", participationWrapper);
        if (participationWrapper.getParticipation().getId() == null) {
            return createParticipation(participationWrapper);
        }
        Participation result = participationRepository.save(participationWrapper.getParticipation());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("participation", participationWrapper.getParticipation().getId().toString()))
            .body(result);
    }

    /**
     * GET  /participations : get all the participations.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of participations in body
     */
    @RequestMapping(value = "/participations",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Participation> getAllParticipations() {
        log.debug("REST request to get all Participations");
        List<Participation> participations = participationRepository.findAll();
        return participations;
    }

    /**
     * GET  /participations/:id : get the "id" participation.
     *
     * @param id the id of the participation to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the participation, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/participations/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Participation> getParticipation(@PathVariable Long id) {
        log.debug("REST request to get Participation : {}", id);
        Participation participation = participationRepository.findOne(id);
        return Optional.ofNullable(participation)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /participations/:id : delete the "id" participation.
     *
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/participations/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteParticipation(@PathVariable List<Long> id) {
        for(Long i: id) {
            log.debug("REST request to delete Participation : {}", i);
            participationRepository.delete(i);
        }
        return ResponseEntity.ok().build();
    }
}
